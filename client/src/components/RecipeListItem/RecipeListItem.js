import React from 'react';
import PropTypes from 'prop-types';
import RecipeThumbnail from "../RecipeThumbnail/RecipeThumbnail";
import './RecipeListItem.css';
import {Button, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";
import OverallRating from "../AverageRatings/OverallRating";


const RecipeListItem = props => {

  return (
    <div className='CardBody'>
      <div className='Card'>
        <div className='Img'>
          <Link to={`/recipes/${props._id}`}>
            <h2>{props.title}</h2>
          </Link>
          <RecipeThumbnail onClick={props.onClick} image={props.image}/>
        </div>
        <div>({props.numOverall}, {props && props.countComment ? props.countComment.count:null} Reviews)</div>
        <OverallRating  numOverall={props.numOverall}/>
      </div>
        {(props.user && props.user.role === 'admin') ? <div>
            <Row>
                <Col sm={4}>
                    {(props.removed === true) ? <h5 style={{color: 'red', textAlign: 'center'}}>
                        <strong>Удалено!</strong>
                    </h5>: null}
                </Col>
                {props.user &&
                <Col sm={2}>
                    <Button
                        style={{margin: '20px'}}
                        color="danger"
                        className="float-right"
                        onClick={() => props.removedRecipe(props._id, {removed: true})}
                    >
                        Remove
                    </Button>
                </Col>
                }
            </Row>
        </div> :null}

    </div>
  );
};

RecipeListItem.propTypes = {
  image: PropTypes.string,
  _id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
};

export default RecipeListItem;
