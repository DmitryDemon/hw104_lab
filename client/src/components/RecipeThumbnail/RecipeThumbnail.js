import React from 'react';

import imageNotAvailable from '../../assets/images/image_not_available.png';
import config from "../../config";

const styles = {
  width: '50%',
  height: '70%',
  marginRight: '10px',
  marginBottom: '20px'
};

const RecipeThumbnail = props => {
  let image = imageNotAvailable;

  if (props.image) {
    image = config.apiURL + '/uploads/' + props.image;
  }

  return <img src={image} style={styles} className="img-thumbnail" alt="Recipe" />
};

export default RecipeThumbnail;
