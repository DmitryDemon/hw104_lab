import React, {Fragment} from 'react';
import StarRatings from "react-star-ratings";
import {Col, Row} from "reactstrap";
import OverallRating from "./OverallRating";


const AverageRatings = (props) => {
    return (
        <Fragment>
            <hr/>
            <Row>
                <Col sm={2}>
                    Overall
                </Col>
                <Col sm={10}>
                    <OverallRating numOverall={props.numOverall}/>
                </Col>
                <Col sm={2}>
                    Easy to make
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numEasy}
                        ratingToShow={props.numEasy}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numEasy}
                </span>
                </Col>

                <Col sm={2}>
                    Quick to make
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numQuick}
                        ratingToShow={props.numQuick}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numQuick}
                </span>
                </Col>
                <Col sm={2}>
                    Taste
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numTaste}
                        ratingToShow={props.numTaste}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numTaste}
                </span>
                </Col>
            </Row>
        </Fragment>


    );
};

export default AverageRatings;