import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';

const UserMenu = ({user, logout}) => {
    const BASE_URL = 'http://localhost:8000/uploads/';
    const src =  BASE_URL + user.avatarImage;
    return (
        <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                <img width='40px' src={src} alt="userImg"/>
                Hello, {user.username}
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem divider/>
                <DropdownItem onClick={logout}>
                    Logout
                </DropdownItem>
                <DropdownItem tag={RouterNavLink} to={"/recipes/new"} exact>
                    Создать новый рецепт
                </DropdownItem>
            </DropdownMenu>
        </UncontrolledDropdown>
        )

};

export default UserMenu;
