import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import RecipeThumbnail from "../../components/RecipeThumbnail/RecipeThumbnail";
import '../../components/RecipeListItem/RecipeListItem.css';
import {fetchOneRecipe} from "../../store/actions/oneRecipeActions";
import './OneRecipe.css';
import AddComment from "../AddComment/AddComment";
import Comments from "../Comments/Comments";
import Slider from "react-slick";
import "./SlickCarousel.css";
import "./OneRecipe.css";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {createPhoto} from "../../store/actions/recipeActions";
import config from "../../config";

class oneRecipe extends Component {
    state={
        gallery: [],
    };

    componentDidMount() {
        this.props.onFetchRecipe(this.props.match.params.userId);
    }

    galleryChangeHandler = event => {
        event.preventDefault();

        let files = Array.from(event.target.files);

        files.forEach((file) => {
            let reader = new FileReader();
            reader.onloadend = () => {
                this.setState({
                    gallery: [...this.state.gallery, file],
                });
            };
            reader.readAsDataURL(file);
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();

        for (let i = 0; i < this.state.gallery.length; i++) {
            formData.append('gallery', this.state.gallery[i]);
        }

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.createPhoto(formData, this.props.match.params.userId);
    };




    render() {
        console.log(this.props.oneRecipe);

        const permissionAddComments = (this.props.oneRecipe && this.props.oneRecipe.user) !==
                                      (this.props.user && this.props.user._id) &&
                                      (this.props.user && this.props.user.role);

        const admissionAddPictures = (this.props.oneRecipe && this.props.oneRecipe.user) ===
                                     (this.props.user && this.props.user._id);
        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1
        };

        return (
            <Fragment>
                {this.props && this.props.oneRecipe ?
                    <div>
                        <h1>
                            {this.props.oneRecipe.title}
                        </h1>
                        <div className='img'>
                            <RecipeThumbnail image={this.props.oneRecipe.image}/>
                            <p>{this.props.oneRecipe.description}</p>
                            <hr/>
                        </div>

                        <div className='images'>
                            <div className="gallery-slider">
                                <Slider {...settings}>
                                    {
                                        this.props.oneRecipe.gallery.map(slider => (
                                            <img key={slider} className='Slide-img' src={config.apiURL + '/uploads/' + slider} alt={this.props.oneRecipe.name}/>
                                        ))
                                    }
                                </Slider>
                            </div>
                        </div>


                            <div className='comments'>
                                <div>
                                    <Comments recipeId={this.props.match.params.userId}/>
                                    {permissionAddComments ?
                                    <AddComment recipeId={this.props.match.params.userId}/>
                                     :null}
                                </div>
                                <hr/>
                            </div>


                        {admissionAddPictures ?
                            <Form onSubmit={this.submitFormHandler}>
                                <FormGroup row>
                                    <Label for="gallery" sm={2}>Галерея</Label>
                                    <Col sm={10}>
                                        <Input
                                            type="file"
                                            name="gallery" id="gallery"
                                            onChange={this.galleryChangeHandler}
                                            multiple
                                        />
                                    </Col>
                                </FormGroup>

                                <FormGroup row>
                                    <Col sm={{offset:2, size: 10}}>
                                        <Button type="submit" color="primary">Add Photo</Button>
                                    </Col>
                                </FormGroup>
                            </Form>
                        :null}


                    </div>

                    : <div>Loading</div>
                }
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    oneRecipe: state.oneRecipe.oneRecipe,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onFetchRecipe: id => dispatch(fetchOneRecipe(id)),
    createPhoto: (recipeData, id) => dispatch(createPhoto(recipeData, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(oneRecipe);
