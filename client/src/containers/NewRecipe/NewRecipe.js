import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import RecipeForm from "../../components/RecipeForm/RecipeForm";
import {createRecipe} from "../../store/actions/recipeActions";


class NewPhoto extends Component {

    createRecipe = recipeData => {
        this.props.onRecipeCreated(recipeData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>New Recipe</h2>
                <RecipeForm
                    onSubmit={this.createRecipe}
                    photos={this.props.photos}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    recipes: state.recipes.recipes,
});

const mapDispatchToProps = dispatch => ({
    onRecipeCreated: recipeData => dispatch(createRecipe(recipeData)),

});

export default connect(mapStateToProps, mapDispatchToProps)(NewPhoto);