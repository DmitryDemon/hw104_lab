import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import 'moment/locale/ru';
import {fetchRecipes, removedRecipe} from "../../store/actions/recipeActions";
import {Col, Row} from "reactstrap";
import RecipeListItem from "../../components/RecipeListItem/RecipeListItem";

class Recipes extends Component {
    componentDidMount() {
        this.props.fetchRecipes(this.props.match.params.userId)
    };

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.userId !== this.props.match.params.userId) {
            this.props.fetchRecipes(this.props.match.params.userId);
        }
    };
    render() {
        return (
            <Fragment>
                <Row>
                    <Col sm={12}>
                        {this.props.recipes.map((recipe,key) => {
                            return (
                                    <Fragment key={key}>
                                        <RecipeListItem
                                            key={recipe._id}
                                            _id={recipe._id}
                                            title={recipe.title}
                                            image={recipe.image}
                                            description={recipe.description}
                                            removed={recipe.removed}
                                            userRecipe={recipe.user}
                                            user={this.props.user}
                                            removedRecipe={this.props.removedRecipe}
                                            numOverall={recipe.numOverall}
                                            countComment={recipe.countComment}
                                        />
                                    </Fragment>

                            )
                        })}
                    </Col>
                </Row>
            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    recipes: state.recipes.recipes,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    fetchRecipes: userId => dispatch(fetchRecipes(userId)),
    removedRecipe: (id, remove) => dispatch(removedRecipe(id, remove)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Recipes);
