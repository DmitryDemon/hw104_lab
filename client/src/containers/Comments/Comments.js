import React, {Component, Fragment} from 'react';
import StarRatings from "react-star-ratings";

import {connect} from "react-redux";
import {Card, CardText, CardTitle, Col, Row} from "reactstrap";
import {fetchComment} from "../../store/actions/commentActions";
import AverageRatings from "../../components/AverageRatings/AverageRatings";
import axios from "../../axios-api";


class Comments extends Component {

    componentDidMount() {
        this.props.fetchComments(this.props.recipeId)
    }

    putOveral = (numOverall,recipeId) => {
        axios.patch(`/recipes`,{
            numOverall,
            recipeId
        })
    };

    render() {

        let numOverall = 0;
        let numEasy = 0;
        let numQuick = 0;
        let numTaste = 0;

        {this.props.comments.map(comment => {
            numEasy += comment.ratingEasyToMake / this.props.comments.length;
            numQuick += comment.ratingQuickToMake / this.props.comments.length;
            numTaste += comment.ratingTaste / this.props.comments.length;
            numOverall = (numEasy + numQuick + numTaste) / 3;

        })}
        this.putOveral(numOverall,this.props.recipeId);
        return (
            <Fragment>
                <AverageRatings
                    numOverall={parseFloat(numOverall.toFixed(1))}
                    numEasy={parseFloat(numEasy.toFixed(1))}
                    numQuick={parseFloat(numQuick.toFixed(1))}
                    numTaste={parseFloat(numTaste.toFixed(1))}
                />
                <h3>Comments</h3>
                {this.props.comments.length > 0 ? this.props.comments.map(comment => (
                    <Card key={comment._id} className="mt-2 p-3">
                        <CardTitle>
                            On {comment.dateTime}, <span>{comment.user.username}:</span>
                        </CardTitle>
                        <CardText>{comment.commentText}</CardText>

                        <Row>
                            <Col sm={2}>
                                Easy to make:
                            </Col>

                            <Col sm={10}>
                                <StarRatings
                                    rating={comment.ratingEasyToMake}
                                    ratingToShow={comment.ratingEasyToMake}
                                    starDimension={'25px'}
                                    starRatedColor="orange"
                                    numberOfStars={5}
                                />

                                <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.ratingEasyToMake}
                             </span>
                            </Col>
                            <Col sm={2}>
                                Quick to make:
                            </Col>

                            <Col sm={10}>
                                <StarRatings
                                    rating={comment.ratingQuickToMake}
                                    ratingToShow={comment.ratingQuickToMake}
                                    starDimension={'25px'}
                                    starRatedColor="orange"
                                    numberOfStars={5}
                                />

                                <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.ratingQuickToMake}
                             </span>
                            </Col>
                            <Col sm={2}>
                                Taste:
                            </Col>
                            <Col sm={10}>
                                <StarRatings
                                    rating={comment.ratingTaste}
                                    ratingToShow={comment.ratingTaste}
                                    starDimension={'25px'}
                                    starRatedColor="orange"
                                    numberOfStars={5}
                                />

                                <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.ratingTaste}
                             </span>
                            </Col>
                        </Row>
                    </Card>
                )): <div>No Comments</div>}

            </Fragment>

        );
    }
}


const mapStateToProps = state => ({
    comments: state.comments.comments
});

const mapDispatchToProps = dispatch => ({
    fetchComments: recipeId => dispatch(fetchComment(recipeId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Comments);