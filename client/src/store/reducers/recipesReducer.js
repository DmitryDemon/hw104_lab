import {FETCH_RECIPE_SUCCESS} from "../actions/recipeActions";
import {FETCH_ONE_RECIPE_SUCCESS} from "../actions/oneRecipeActions";


const initialState = {
  recipes: [],
  oneRecipe: null,
};

const recipesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_RECIPE_SUCCESS:
      return {...state, recipes: action.recipes};
    case FETCH_ONE_RECIPE_SUCCESS:
      return {...state, oneRecipe: action.recipes};
    default:
      return state;
  }
};

export default recipesReducer;
