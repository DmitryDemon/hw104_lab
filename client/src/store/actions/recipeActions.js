import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";
import {push} from "connected-react-router";


export const FETCH_RECIPE_SUCCESS = 'FETCH_RECIPE_SUCCESS';
export const CREATE_RECIPE_SUCCESS = 'CREATE_RECIPE_SUCCESS';

export const fetchRecipesSuccess = recipes => ({type: FETCH_RECIPE_SUCCESS, recipes});
export const createRecipeSuccess = () => ({type: CREATE_RECIPE_SUCCESS});


export const fetchRecipes = userId => {
    return dispatch => {
        let url = '/recipes';
        if (userId) {
            url += '?user=' + userId
        }
        return axios.get(url).then(
            response => dispatch(fetchRecipesSuccess(response.data))
        );
    };
};

export const createRecipe = recipeData => {
    return dispatch => {
        return axios.post('/recipes', recipeData).then(
            () => dispatch(createRecipeSuccess())
        );
    };
};

export const createPhoto = (recipeData, id, redirect = true) => {
    return dispatch => {
        return axios.post(`/recipes/${id}`, recipeData).then(
            response => {
                dispatch(createRecipeSuccess(response.data));
                if (redirect) {
                    dispatch(push('/recipes/' + response.data._id));
                }
            },
            error => {

            }
        )
    }
};

export const removedRecipe = (id, remove) => {
    return dispatch => {
        return axios.post(`/recipes/${id}/toggle_removed`, remove).then(
            () => {
                dispatch(fetchRecipes());
                NotificationManager.warning('You removed an cocktail!');
            }
        );
    };
};



