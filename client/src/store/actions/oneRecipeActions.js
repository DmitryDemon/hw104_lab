import axios from '../../axios-api';

export const FETCH_ONE_RECIPE_SUCCESS = 'FETCH_ONE_RECIPE_SUCCESS';


export const fetchOneRecipeSuccess = recipes => ({type: FETCH_ONE_RECIPE_SUCCESS, recipes});



export const fetchOneRecipe = id => {
    return (dispatch) => {
        return axios.get(`/recipes/${id}`).then(
            response => {
                dispatch(fetchOneRecipeSuccess(response.data));
            }
        );
    };
};

