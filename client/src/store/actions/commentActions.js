import axios from '../../axios-api';

export const FETCH_COMMENT_SUCCESS = 'FETCH_COMMENT_SUCCESS';

export const fetchCommentSuccess = comments => ({type: FETCH_COMMENT_SUCCESS, comments});

export const fetchComment = id => {
    return (dispatch) => {
        return axios.get(`/comments?recipeId=${id}`).then(
            response => {
                dispatch(fetchCommentSuccess(response.data));
            }
        );
    };
};

export const createComment = (comment) => {
    return dispatch => {
        return axios.post('/comments',comment).then(
            (result) => {
                dispatch(fetchComment(comment.recipeId))
            }
        );
    };
};
