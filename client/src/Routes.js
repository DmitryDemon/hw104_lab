import React from 'react';
import {Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";


import Recipes from "./containers/Recipes/Recipes";
import NewRecipe from "./containers/NewRecipe/NewRecipe";
import OneRecipe from "./containers/OneRecipe/OneRecipe";



const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={Recipes}/>
      <Route path="/recipes/new" exact component={NewRecipe}/>
      <Route path="/recipes/:userId" exact component={OneRecipe}/>
      <Route path="/register" exact component={Register}/>
      <Route path="/login" exact component={Login}/>
    </Switch>
  );
};

export default Routes;