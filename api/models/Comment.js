const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const CommentSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    recipeId: {
        type: Schema.Types.ObjectId,
        ref: 'Recipe',
        required: true,
    },
    commentText: {
        type: String,
        required:true,
    },
    dateTime: String,
    ratingEasyToMake: Number,
    ratingQuickToMake: Number,
    ratingTaste: Number,


});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;
