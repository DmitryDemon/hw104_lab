const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const RecipeSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required:true,
    },
    description: String,
    removed: {
        type: Boolean,
        required: true,
        default: false
    },
    numOverall: Number,
    gallery: [String],
});

const Recipe = mongoose.model('Recipe', RecipeSchema);

module.exports = Recipe;