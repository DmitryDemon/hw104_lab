exports.config = {
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'http://localhost:3010',
      show: !process.env.CI,
      headless: !!process.env.CI,
      windowSize: "1024x768",
    },
    LoginHelper: {
      require: './helpers/login.js',
    },
    GeoHelper: {
      require: './helpers/geo.js'
    },
    WordBookHelper: {
      require: './wordBook/wordBook.js'
    }
  },
  include: {
    I: './steps_file.js'
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: [
        './wordBook/wordBook.js',
        './step_definitions/register.js',
        './step_definitions/login.js',
        './step_definitions/createEvent.js',
        './step_definitions/addDeleteCategory.js',
    ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    }
  },
  tests: './*_test.js',
  name: 'tests'
};