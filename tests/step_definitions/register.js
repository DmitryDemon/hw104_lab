const { I } = inject();
// Add in your custom step files


Given('я нохожусь на странице регистрации', () => {
    I.amOnPage(link.register)

});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
    I.waitForElement({xpath: '' + `//input[@id=${fieldName}]`}, 30);
    I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});


When('я загружаю аватарку', () => {
    I.attachFile({xpath:"//input[@id='avatarImage']"}, 'dataImage/jw.jpeg');
});

Then('кликаю на кнопку {string}', () => {
    I.click(button.checkIn);
});

When('я вижу оповещение {string}', (text) => {
    I.seeNotification(text)
});


When('я вижу имя пользователя {string}', (name) => {
    I.waitForElement(`//a[contains(text(), 'Привет, ${name}']`, 5);
});
