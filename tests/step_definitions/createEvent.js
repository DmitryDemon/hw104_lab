const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
    I.loginAsUser(name);
});

When('я нахожусь на странице создания ивента', () => {
    I.amOnPage(link.addEvent);
    I.waitForText('Добавить новый курс', 2);
});

When('я ввожу поля формы:', table => {
    for(const id in table.rows){
        if(id<1){
            continue;
        }
        const cells = table.rows[id].cells;

        const name = cells[0].value;
        const value = cells[1].value;

        if (name === "image" || name === "logo") {
            I.attachFile(`//input[@id='${name}']`,  'dataImage/jw.jpeg');
        } else{
            I.fillField({xpath: `//*[@id='${name}']`}, value);
        }
    }
});

When('нажимаю на календарь для выбора даты', () => {
   I.click("//button[contains(@class,'react-date-picker__calendar-button')]")
});

When('выбираю категорию {string}', (optionName) => {
    I.selectOption("//[@name='category']", optionName)

});

When('выбираю дату начала {string}', (date) => {
    I.click(`//abbr[.='${date}']`);
});

When('выберем позицию на карте', () => {
   I.click("//div[contains(@class,'leaflet-container')]");
});

When('кликаю по кнопке {string}', () => {
    I.click(button.saveEvent);
});

When('я вижу созданный ивент {string} на главной странице', (eventName) => {
    I.amOnPage(link.home);
    I.waitForText(eventName);
});

When('я вижу что ивент {string} в статусе {string}',(eventName,eventStatus) =>{
   I.waitForElement(`//*[contains(text(), "${eventName}")]
                    /ancestor::div[contains(@class,'card')]//h5[contains(text(), "${eventStatus}")]`, 10);
});

