const { I } = inject();
// Add in your custom step files

Given('я нохожусь на странице Авторизация', () => {
    I.amOnPage(link.login)
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
    I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});

When('нажимаю на кнопку {string}', () => {
    I.click(button.logIn)
});

Then('я вижу оповещение {string}', (text) => {
    I.seeNotification(text);
});

When('я вижу имя пользователя {string}', (name) => {
    I.waitForElement(`//a[contains(., 'Привет, ${name}!')]`, 10)
});