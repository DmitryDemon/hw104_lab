const { I } = inject();
// Add in your custom step files

Given('я зарегистрированный пользователь {string}', (name) => {
    I.loginAsUser(name);
});


When('я открываю страницу {string}', () => {
    I.wait(2);
    I.click(button.menuUser);
    I.wait(2);
    I.click(button.categoryManagement);
});

When('я вижу и кликаю на кнопку {string}', () => {
   I.click(button.addCategory);
});

When('я ввожу в текстовое поле название категории на русском языке {string}', (categoryName) => {
    I.wait(2);
    I.fillField(`//input[@id='titleRu']`, categoryName);
});

When('я ввожу в текстовое поле название категории на кыргызском языке {string}', (categoryName) => {
    I.fillField(`//input[@id='titleKy']`, categoryName);
});

When('я ввожу в текстовое поле название категории на английском языке {string}', (categoryName) => {
    I.fillField(`//input[@id='titleEn']`, categoryName);
});

When('я затем нажимаю на кнопку {string}', () => {
    I.click(button.saveEvent);
});

When('я нажимаю на кнопку {string} ', () => {
   I.click(`//td[.='Тестовая категория']/parent::tr/child::td/button`);
});

When('я вижу текст что {string}', (text) => {
    I.seeNotification(text);
});