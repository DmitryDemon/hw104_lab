# language: ru


Функция: Регистрация пользователя
  @register
  Сценарий: Успешная регистрация
    Допустим я нохожусь на странице регистрации
    Если я ввожу "John Doe" в поле "displayName"
    И я ввожу "John01@gmail.com" в поле "email"
    И я ввожу "123" в поле "password"
    И я ввожу "+996553001012" в поле "phone"
    И я загружаю аватарку
    То кликаю на кнопку "Регистрация"
    И я вижу оповещение "Успешно зарегистрированы!"
    И я вижу имя пользователя "John Doe"