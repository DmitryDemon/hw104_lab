const Helper = codeceptjs.helper;

eventData = {
  "JavaScript": 'JavaScript курсы. Научись программировать с нуля',
  "TheorMech": 'Теоретическая механика для инженеров и исследователей',
  "Chem": 'Химия-курсы для всех возрастов',
  "TestEvent": 'Карточка для автотеста по редактированию курса',
  "WebDev": "Курс по веб разработки"
};

button = {
  menuUser: "//*[@id='root']/header/nav/ul/li/a/span", //выпадающее меню пользователя
  myCalendar: "//*[@id='root']/header/nav/ul/li/div/a[1]/span", //кнопка просмотра календаря пользователя,
  viewCourse: "button/span[.='Просмотреть курс']", //кнопка просмотреть курс
  output: "//button[contains(text(),'Выход')]", //кнопка выход для разлогинивания
  logIn: "//button[.='Войти']", //кнопка войти для логина
  checkIn: "//button[.='Регистрация']", //кнопка регистрации пользователя
  addGroup: "//a[.='Добавить группу']", //кнопка добавления группы
  addedGroup: "//button[contains(., 'Добавить')]", //кнопка Добавить в форме добавления групп
  deleteGroup: "button[.='Удалить']", //кнопка удаления группы
  signUp: "//button[.='Записаться']", //кнопка записи на крус
  onModeration: "//button[.='На модерации']", //кнопка На модерации
  published: "//button[.='Опубликовать']", //кнопка Опубликовать
  sendOnModeration: "//button[.='Отправить на модерацию']", //кнопка отправить на модерацию
  viewListUsers: "//a[.='Просмотр пользователей']", //кнопка просмотра списка пользователей
  categoryManagement: "//a/span[.='Упраление категориями']", //кнопка управления категориями
  editEvent: "//a[.='Редактировать']", //кнопка редактировать
  saveEvent: "//button[.='Добавить']", //кнопка "Добавить" после редактирования евента
  viewCalendar: "//a[.='Просмотреть расписание']", //кнопка просмотра календаря
  addCategory: "//button/span[.='Добавить категорию']" //кнопка добавить категорию
};

formGroup = {
  title: "//*[@id='name']",

};

link = {
  home: '/',
  login: '/login',
  register: '/register',
  addEvent: '/events/new',
};

class WordBookHelper extends Helper {

  async seeNotification(text) {
    const I = this.helpers['Puppeteer'];
    await I.waitForElement(`//div[contains(@class, 'notification-container')]/span[.='${text}']`, 10);
  };

  async clickViewCourses(text) {
    const title = eventData[text];
    const I = this.helpers['Puppeteer'];
    await I.wait(2);
    await I.click(`//h2[contains(.,'${title}')]/ancestor::div[@class='card-body']/descendant::${button.viewCourse}`);
  }

}

module.exports = WordBookHelper;