
const Helper = codeceptjs.helper;

const userData = {
  'Killer': {
    'email': 'John.Week@gmail.com',
    'password': '123'
  },
  'Haizenberg': {
    'email': 'Ad.Haizenberg@gmail.com',
    'password': '123'
  },
  'some2': {
    'email': 'some2@gmail.com',
    'password': '123'
    },
};



class LoginHelper extends Helper {

  async loginAsUser(name) {
    const user = userData[name];

    if (!user) throw new Error('No such user is known! Check helpers/login.js');

    const I = this.helpers['Puppeteer'];

    await I.amOnPage('/login');

    const alreadyLoggedIn = await I._locate(`//a[contains(text(), 'Привет, ${name}')]`);

    if (alreadyLoggedIn.length > 0) {
      return;
    }

    const someoneIsLoggedIn = await I._locate(`//a[contains(text(), 'Привет,')]`);

    if (someoneIsLoggedIn.length > 0) {
      await I.click(button.menuUser);
      await I.click(button.output);
    }

    await I.fillField(`//input[@id='email']`, user.email);
    await I.fillField(`//input[@id='password']`, user.password);

    await I.click(button.logIn);

    await I.waitForText(`Привет, ${name}`, 2);
  }
}

module.exports = LoginHelper;
